const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is requried!"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

module.exports = mongoose.model("Task", taskSchema);